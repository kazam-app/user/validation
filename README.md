# Validation Interface
[![build status](https://git.urbanity.xyz/kazam/user/validation/badges/master/build.svg)](https://git.urbanity.xyz/kazam/user/validation/commits/master)

## Dependencies
- yarn
- gulp

## Install

```sh
yarn install
```

## Run

```sh
gulp
```

## Build

```sh
gulp build
```

## Deploy

AWS S3
